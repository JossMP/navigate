<?php

require __DIR__ . '/../vendor/autoload.php';

use jossmp\navigate\Curl;

// curl "https://httpbin.org/post" -d "foo[]=bar&foo[]=baz"

$curl = new Curl();
$curl->post('https://httpbin.org/post', [
    'foo' => [
        'bar',
        'baz',
    ],
]);
